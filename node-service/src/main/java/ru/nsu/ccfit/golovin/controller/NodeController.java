package ru.nsu.ccfit.golovin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.nsu.ccfit.golovin.entity.Node;
import ru.nsu.ccfit.golovin.service.NodeService;

import java.util.List;

@RestController
public class NodeController {
    private NodeService nodeService;

    @Autowired
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @RequestMapping(value = "/node-service/entity/node/{id}", method = RequestMethod.GET)
    public ResponseEntity<Node> findById(@PathVariable("id") Long id) {
        Node node = nodeService.findOne(id);
        if (node == null) {
            return new ResponseEntity<Node>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Node>(node, HttpStatus.OK);
    }

    @RequestMapping(value = "/node-service/entity/node", method = RequestMethod.GET, params = { "osmId" })
    public ResponseEntity<List<Node>> findByOsmId(@RequestParam("osmId") Long osmId) {
        List<Node> nodes = nodeService.findByOsmId(osmId);
        if (nodes.isEmpty()) {
            return new ResponseEntity<List<Node>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Node>>(nodes, HttpStatus.OK);
    }

    @RequestMapping(value = "/node-service/entity/node", method = RequestMethod.GET, params = { "tagK", "tagV" })
    public ResponseEntity<List<Node>> findByTagKAndTagV(@RequestParam("tagK") String k, @RequestParam("tagV") String v) {
        List<Node> nodes = nodeService.findByTagKAndTagV(k, v);
        if (nodes.isEmpty()) {
            return new ResponseEntity<List<Node>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Node>>(nodes, HttpStatus.OK);
    }

    @RequestMapping(value = "/node-service/service/nodesInDistanceFrom", method = RequestMethod.GET, params = { "lat", "lon", "distance" })
    public ResponseEntity<List<Node>> findNodesInDistanceFrom(@RequestParam("lat") Double lat, @RequestParam("lon") Double lon,
                                                              @RequestParam("distance") Double distance) {
        List<Node> nodes = nodeService.findNodesInDistanceFrom(lat, lon, distance);
        if (nodes.isEmpty()) {
            return new ResponseEntity<List<Node>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Node>>(nodes, HttpStatus.OK);
    }

    @RequestMapping(value = "/node-service/entity/node/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Node> delete(@PathVariable Long id) {
        Node node = nodeService.findOne(id);
        if (node == null) {
            return new ResponseEntity<Node>(HttpStatus.NOT_FOUND);
        }

        nodeService.delete(id);

        return new ResponseEntity<Node>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/node-service/entity/node", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody Node node, UriComponentsBuilder ucBuilder) {
        Node existingNode = nodeService.findOne(node.getId());
        if (existingNode != null) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        nodeService.save(node);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/node-service/entity/node/{id}").buildAndExpand(node.getId()).toUri());

        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/node-service/entity/node/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Node> update(@PathVariable("id") Long id, @RequestBody Node node) {
        Node currentNode = nodeService.findOne(id);

        if (currentNode == null) {
            return new ResponseEntity<Node>(HttpStatus.NOT_FOUND);
        }

        currentNode.setLat(node.getLat());
        currentNode.setLon(node.getLon());
        currentNode.setUserName(node.getUserName());
        currentNode.setUid(node.getUid());
        currentNode.setVisible(node.getVisible());
        currentNode.setVersion(node.getVersion());
        currentNode.setChangeset(node.getChangeset());
        currentNode.setTimestamp(node.getTimestamp());
        currentNode.setTags(node.getTags());

        nodeService.update(node);

        return new ResponseEntity<Node>(node, HttpStatus.OK);
    }
}
