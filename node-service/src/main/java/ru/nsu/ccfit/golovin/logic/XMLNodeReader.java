package ru.nsu.ccfit.golovin.logic;

import org.openstreetmap.osm._0.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.IOException;
import java.io.InputStream;

public class XMLNodeReader {
    private final Logger logger;
    private final XMLStreamReader xmlStreamReader;
    private final Unmarshaller jaxbUnmarshaller;

    public XMLNodeReader(InputStream inputStream) throws IOException, XMLStreamException, JAXBException, SAXException {
        logger = LoggerFactory.getLogger(XMLNodeReader.class);

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        xmlStreamReader = new XMLNSStreamReader(xmlInputFactory.createXMLStreamReader(inputStream));

        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    }

    public Node nextNode() {
        try {
            while (xmlStreamReader.hasNext()) {
                if (xmlStreamReader.isStartElement() &&
                        "node".equals(xmlStreamReader.getName().toString())) {
                    return (Node) jaxbUnmarshaller.unmarshal(xmlStreamReader);
                }

                xmlStreamReader.next();
            }

            try {
                xmlStreamReader.close();
            } catch (XMLStreamException e) {
                logger.error(e.getMessage());
            }
        } catch (JAXBException | XMLStreamException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    private static class XMLNSStreamReader extends StreamReaderDelegate {
        public XMLNSStreamReader(XMLStreamReader reader) {
            super(reader);
        }

        @Override
        public String getNamespaceURI() {
            return "http://openstreetmap.org/osm/0.6";
        }
    }
}
