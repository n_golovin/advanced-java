package ru.nsu.ccfit.golovin;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import ru.nsu.ccfit.golovin.logic.NodeStorageLoader;
import ru.nsu.ccfit.golovin.model.Statistics;

@SpringBootApplication
@ConfigurationProperties(locations = "classpath:/application.yml")
public class Main {
    @Bean
    CommandLineRunner init(/*NodeStorageLoader nodeStorageLoader*/) {
        return (args)-> {
//            Statistics statistics = nodeStorageLoader.extractFromXmlFileAndSaveNodes(ClassLoader.getSystemResource("TM-160501.osm.bz2").getPath());
//
//            statistics.printUserEditsCountsStatistic();
//            statistics.printKeysInNodesCountsStatistic();
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
