package ru.nsu.ccfit.golovin.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.PackageVersion;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.ccfit.golovin.entity.Node;

import java.io.IOException;

public class NodeSerializer extends JsonSerializer<Node> {
    Logger logger = LoggerFactory.getLogger(NodeSerializer.class);

    @Override
    public void serialize(Node value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeStartObject();
        gen.writeObjectFieldStart(value.getClass().getName());

        if (value.getId() == null) {
            gen.writeNullField("id");
        } else {
            gen.writeNumberField("id", value.getId());
        }
        if (value.getOsmId() == null) {
            gen.writeNullField("osmId");
        } else {
            gen.writeNumberField("osmId", value.getOsmId());
        }
        if (value.getLat() == null) {
            gen.writeNullField("lat");
        } else {
            gen.writeNumberField("lat", value.getLat());
        }
        if (value.getLon() == null) {
            gen.writeNullField("lon");
        } else {
            gen.writeNumberField("lon", value.getLon());
        }
        if (value.getUserName() == null) {
            gen.writeNullField("userName");
        } else {
            gen.writeStringField("userName", value.getUserName());
        }
        if (value.getUid() == null) {
            gen.writeNullField("uid");
        } else {
            gen.writeNumberField("uid", value.getUid());
        }
        if (value.getVisible() == null) {
            gen.writeNullField("visible");
        }
        else {
            gen.writeBooleanField("visible", value.getVisible());
        }
        if (value.getVersion() == null) {
            gen.writeNullField("version");
        } else {
            gen.writeNumberField("version", value.getVersion());
        }
        if (value.getChangeset() == null) {
            gen.writeNullField("changeset");
        } else {
            gen.writeNumberField("changeset", value.getChangeset());
        }
        if (value.getTimestamp() == null) {
            gen.writeNullField("timestamp");
        } else {
            gen.writeStringField("timestamp", value.getTimestamp().toString());
        }
        gen.writeArrayFieldStart("tags");
        value.getTags().forEach(tag -> {
            try {
                gen.writeObject(tag);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        });

        gen.writeEndArray();
        gen.writeEndObject();
        gen.writeEndObject();
    }
}
