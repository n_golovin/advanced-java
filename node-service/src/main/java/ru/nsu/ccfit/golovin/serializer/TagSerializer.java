package ru.nsu.ccfit.golovin.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import ru.nsu.ccfit.golovin.entity.Tag;

import java.io.IOException;

public class TagSerializer extends JsonSerializer<Tag> {
    @Override
    public void serialize(Tag value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeStartObject();
        gen.writeObjectFieldStart(value.getClass().getName());

        if (value.getId() == null) {
            gen.writeNullField("id");
        } else {
            gen.writeNumberField("id", value.getId());
        }
        if (value.getK() == null) {
            gen.writeNullField("k");
        } else {
            gen.writeStringField("k", value.getK());
        }
        if (value.getV() == null) {
            gen.writeNullField("v");
        } else {
            gen.writeStringField("v", value.getV());
        }

        gen.writeEndObject();
        gen.writeEndObject();
    }
}
