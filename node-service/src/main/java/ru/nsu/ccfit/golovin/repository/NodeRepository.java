package ru.nsu.ccfit.golovin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.golovin.entity.Node;

import java.util.List;

@Repository
public interface NodeRepository extends JpaRepository<Node, Long> {
    List<Node> findByTags_kAndTags_v(String k, String v);

    List<Node> findByOsmId(Long osmId);

    @Query("SELECT n FROM Node n \n" +
            "WHERE ABS(earth_distance(ll_to_earth(n.lat, n.lon), ll_to_earth(0, 0)) - earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(0, 0))) < :distance AND \n" +
            "ABS(earth_distance(ll_to_earth(n.lat, n.lon), ll_to_earth(90, 0)) - earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(90, 0))) < :distance \n" +
            "ORDER BY ABS(earth_distance(ll_to_earth(n.lat, n.lon), ll_to_earth(0, 0)) - earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(0, 0))) + " +
            "ABS(earth_distance(ll_to_earth(n.lat, n.lon), ll_to_earth(90, 0)) - earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(90, 0)))\n")
    List<Node> findNodesInDistanceFrom(@Param("lat") Double lat, @Param("lon") Double lon, @Param("distance") Double distance);
}
