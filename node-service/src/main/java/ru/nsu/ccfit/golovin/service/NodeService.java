package ru.nsu.ccfit.golovin.service;

import ru.nsu.ccfit.golovin.entity.Node;

import java.util.List;

public interface NodeService {
    Node save(Node node);

    List<Node> save(List<Node> nodes);

    Node update(Node node);

    Node findOne(Long id);

    List<Node> findByOsmId(Long osmId);

    List<Node> findByTagKAndTagV(String k, String v);

    List<Node> findNodesInDistanceFrom(Double lat, Double lon, Double distance);

    void delete(Long id);

    void delete(Node node);
}
