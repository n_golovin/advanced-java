package ru.nsu.ccfit.golovin.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;

public class Statistics {
    private final Logger logger = LoggerFactory.getLogger(Statistics.class);

    private HashMap<String, Integer> usersEditsCounts;
    private HashMap<String, Integer> keysInNodesCounts;

    public Statistics() {
        usersEditsCounts = new HashMap<>();
        keysInNodesCounts = new HashMap<>();
    }

    public Statistics(HashMap<String, Integer> usersEditsCounts, HashMap<String, Integer> keysInNodesCounts) {
        this.usersEditsCounts = usersEditsCounts;
        this.keysInNodesCounts = keysInNodesCounts;
    }

    public HashMap<String, Integer> getUsersEditsCounts() {
        return usersEditsCounts;
    }

    public void setUsersEditsCounts(HashMap<String, Integer> usersEditsCounts) {
        this.usersEditsCounts = usersEditsCounts;
    }

    public HashMap<String, Integer> getKeysInNodesCounts() {
        return keysInNodesCounts;
    }

    public void setKeysInNodesCounts(HashMap<String, Integer> keysInNodesCounts) {
        this.keysInNodesCounts = keysInNodesCounts;
    }

    public void printUserEditsCountsStatistic() {
        logger.info("---------------------------------------------------------------------------------------------");
        logger.info("Users edits counts stats:");
        usersEditsCounts.entrySet().stream()
                .sorted(comparing(Map.Entry::getValue, reverseOrder()))
                .forEach(usersEditsEntry -> logger.info("user: " + usersEditsEntry.getKey() +
                        ", edits count: " + usersEditsEntry.getValue()));
    }

    public void printKeysInNodesCountsStatistic() {
        logger.info("---------------------------------------------------------------------------------------------");
        logger.info("Keys in nodes counts stats:");
        keysInNodesCounts.forEach((key, countInNodes) -> logger.info("key: " + key + ", count in nodes: " + countInNodes));
    }
}
