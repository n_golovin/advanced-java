package ru.nsu.ccfit.golovin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.ccfit.golovin.entity.Node;
import ru.nsu.ccfit.golovin.repository.NodeRepository;
import ru.nsu.ccfit.golovin.service.NodeService;

import java.util.List;

@Service
@Transactional
public class JpaNodeService implements NodeService {
    @Autowired
    private NodeRepository nodeRepository;

    @Override
    public Node save(Node node) {
        return nodeRepository.save(node);
    }

    @Override
    public List<Node> save(List<Node> nodes) {
        return nodeRepository.save(nodes);
    }

    @Override
    public Node update(Node node) {
        return nodeRepository.save(node);
    }

    @Override
    @Transactional(readOnly = true)
    public Node findOne(Long id) {
        return nodeRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Node> findByOsmId(Long osmId) {
        return nodeRepository.findByOsmId(osmId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Node> findByTagKAndTagV(String k, String v) {
        return nodeRepository.findByTags_kAndTags_v(k, v);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Node> findNodesInDistanceFrom(Double lat, Double lon, Double distance) {
        return nodeRepository.findNodesInDistanceFrom(lat, lon, distance);
    }

    @Override
    public void delete(Long id) {
        nodeRepository.delete(id);
    }

    @Override
    public void delete(Node node) {
        nodeRepository.delete(node);
    }
}
