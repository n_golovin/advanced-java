package ru.nsu.ccfit.golovin.logic;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.openstreetmap.osm._0.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import ru.nsu.ccfit.golovin.model.Statistics;
import ru.nsu.ccfit.golovin.service.NodeService;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class NodeStorageLoader {
    private static final int BATCH_SIZE = 1000;

    private final Logger logger;

    @Autowired
    private NodeService nodeService;

    public NodeStorageLoader() throws SQLException, ClassNotFoundException, SAXException, XMLStreamException, JAXBException, IOException {
        logger = LoggerFactory.getLogger(NodeStorageLoader.class);
    }

    public Statistics extractFromXmlFileAndSaveNodes(String filename) {
        try (FileInputStream fin = new FileInputStream(filename);
             BufferedInputStream in = new BufferedInputStream(fin);
             BZip2CompressorInputStream bz2cis = new BZip2CompressorInputStream(in)) {
            XMLNodeReader xmlNodeReader = new XMLNodeReader(bz2cis);

            Node xmlNode;
            HashMap<String, Integer> usersEditsCounts = new HashMap<>();
            HashMap<String, Integer> keysInNodesCounts = new HashMap<>();
            List<ru.nsu.ccfit.golovin.entity.Node> nodes = new ArrayList<>(BATCH_SIZE);

            while ((xmlNode = xmlNodeReader.nextNode()) != null) {
                usersEditsCounts.put(xmlNode.getUserName(), usersEditsCounts.get(xmlNode.getUserName()) == null ?
                        1 : usersEditsCounts.get(xmlNode.getUserName()) + 1);

                xmlNode.getTag().forEach(tag -> keysInNodesCounts.put(tag.getK(), keysInNodesCounts.get(tag.getK()) == null ?
                        1 : keysInNodesCounts.get(tag.getK()) + 1));

                ru.nsu.ccfit.golovin.entity.Node node = new ru.nsu.ccfit.golovin.entity.Node(xmlNode);

                nodes.add(node);

                if (nodes.size() == BATCH_SIZE) {
                    nodeService.save(nodes);

                    nodes.clear();
                }
            }

            return new Statistics(usersEditsCounts, keysInNodesCounts);

        } catch (JAXBException | IOException | XMLStreamException | SAXException e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
