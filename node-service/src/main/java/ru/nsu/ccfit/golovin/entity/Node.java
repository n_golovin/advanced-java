package ru.nsu.ccfit.golovin.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.nsu.ccfit.golovin.serializer.NodeSerializer;

import javax.persistence.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonSerialize(using = NodeSerializer.class)
public class Node {
    @Id
    @GeneratedValue
    private Long id;

    private Long osmId;

    private Double lat;

    private Double lon;

    private String userName;

    private Long uid;

    private Boolean visible;

    private Long version;

    private Long changeset;

    @Column(columnDefinition = "timestamp with time zone")
    private Timestamp timestamp;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "node")
    private Set<Tag> tags = new HashSet<>();

    public Node() {
    }

    public Node(org.openstreetmap.osm._0.Node node) {
        this.osmId = node.getId().longValue();
        this.lat = node.getLat();
        this.lon = node.getLon();
        this.userName = node.getUserName();
        this.uid = node.getUid().longValue();
        this.visible = node.isVisible();
        this.version = node.getVersion().longValue();
        this.changeset = node.getChangeset().longValue();
        this.timestamp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());

        node.getTag().forEach(tag -> tags.add(new Tag(tag, this)));
    }

    public Node(Long osmId, Double lat, Double lon, String userName, Long uid, Boolean visible,
                Long version, Long changeset, XMLGregorianCalendar timestamp) {
        this.osmId = osmId;
        this.lat = lat;
        this.lon = lon;
        this.userName = userName;
        this.uid = uid;
        this.visible = visible;
        this.version = version;
        this.changeset = changeset;
        this.timestamp = new Timestamp(timestamp.toGregorianCalendar().getTimeInMillis());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOsmId() {
        return osmId;
    }

    public void setOsmId(Long osmId) {
        this.osmId = osmId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getChangeset() {
        return changeset;
    }

    public void setChangeset(Long changeset) {
        this.changeset = changeset;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "lat: " + getLat(),
                "lon: " + getLon(),
                "userName: " + getUserName(),
                "uid: " + getUid(),
                "visible: " + getVisible(),
                "version: " + getVersion(),
                "changeset: " + getChangeset(),
                "timestamp: " + getTimestamp());
    }
}
