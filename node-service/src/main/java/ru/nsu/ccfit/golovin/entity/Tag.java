package ru.nsu.ccfit.golovin.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.nsu.ccfit.golovin.serializer.TagSerializer;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"k", "node_id"}))
@JsonSerialize(using = TagSerializer.class)
public class Tag {
    @Id
    @GeneratedValue
    private Long id;

    private String k;

    private String v;

    @ManyToOne(cascade = CascadeType.ALL)
    private Node node;

    public Tag() {
    }

    public Tag(String k, String v) {
        this.k = k;
        this.v = v;
    }

    public Tag(String k, String v, Node node) {
        this.k = k;
        this.v = v;
        this.node = node;
    }

    public Tag(org.openstreetmap.osm._0.Tag tag, Node node) {
        this(tag.getK(), tag.getV(), node);
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "k: " + getK(),
                "v: " + getV(),
                "node" + getNode());
    }
}
