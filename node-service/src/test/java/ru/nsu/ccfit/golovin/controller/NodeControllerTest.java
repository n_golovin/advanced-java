package ru.nsu.ccfit.golovin.controller;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.nsu.ccfit.golovin.Main;
import ru.nsu.ccfit.golovin.entity.Node;
import ru.nsu.ccfit.golovin.service.NodeService;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class NodeControllerTest {
    static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Autowired
    WebApplicationContext context;

    MockMvc mockMvc;
    NodeService nodeServiceMock;
    Node patricNode;

    Logger logger = LoggerFactory.getLogger(NodeControllerTest.class);

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).dispatchOptions(true).build();

        this.patricNode = new Node();
        patricNode .setId(1L);
        patricNode .setUserName("Patric");

        this.nodeServiceMock = mock(NodeService.class);
        when(nodeServiceMock.findOne(1L)).thenReturn(patricNode);
        NodeController nodeController = context.getBean(NodeController.class);
        nodeController.setNodeService(nodeServiceMock);
    }

    @Test
    public void findByIdTest() throws Exception {
        mockMvc.perform(get("/node-service/entity/node/" + patricNode.getId())).
                andExpect(status().isOk()).
                andExpect(content().contentType(APPLICATION_JSON_UTF8)).
                andExpect(jsonPath("$.['ru.nsu.ccfit.golovin.entity.Node'].id", is(1))).
                andExpect(jsonPath("$.['ru.nsu.ccfit.golovin.entity.Node'].userName", is(patricNode.getUserName())));

        verify(nodeServiceMock, times(1)).findOne(1L);
        verifyNoMoreInteractions(nodeServiceMock);
    }

    @Test
    public void updateTest() throws Exception {
        Node updatedNode = new Node();
        updatedNode.setId(1L);
        updatedNode.setUserName("Bill");

        mockMvc.perform(put("/node-service/entity/node/1").contentType(APPLICATION_JSON_UTF8).content(new Gson().toJson(updatedNode))).
                andExpect(status().isOk()).
                andExpect(content().contentType(APPLICATION_JSON_UTF8)).
                andExpect(jsonPath("$.['ru.nsu.ccfit.golovin.entity.Node'].id", is(1))).
                andExpect(jsonPath("$.['ru.nsu.ccfit.golovin.entity.Node'].userName", is("Bill")));

        verify(nodeServiceMock, times(1)).findOne(1L);
    }

    @Test
    public void deleteTest() throws Exception {
        mockMvc.perform(delete("/node-service/entity/node/" + patricNode.getId())).
                andExpect(status().isNoContent());

        verify(nodeServiceMock, times(1)).findOne(1L);
        verify(nodeServiceMock, times(1)).delete(1L);
        verifyNoMoreInteractions(nodeServiceMock);
    }

    @Test
    public void createTest() throws Exception {
        Node node = new Node();
        node.setUserName("Bill");

        mockMvc.perform(post("/node-service/entity/node").contentType(APPLICATION_JSON_UTF8).content(new Gson().toJson(node))).
                andExpect(status().isCreated());
    }
}
