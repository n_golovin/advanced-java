package photocenter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import photocenter.repository.*;
import photocenter.service.ClientService;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class DBTest {
    private final Logger logger = LoggerFactory.getLogger(DBTest.class);

    @Autowired
    WebApplicationContext context;

    @Test
    public void test() throws Exception {
        ClientRepository clientRepository = context.getBean(ClientRepository.class);
        DepartmentRepository departmentRepository = context.getBean(DepartmentRepository.class);
        DepartmentTypeRepository departmentTypeRepository = context.getBean(DepartmentTypeRepository.class);
        PostRepository postRepository = context.getBean(PostRepository.class);
        ProductRealizationRepository productRealizationRepository = context.getBean(ProductRealizationRepository.class);
        ProductRepository productRepository = context.getBean(ProductRepository.class);
        ServiceRealizationRepository serviceRealizationRepository = context.getBean(ServiceRealizationRepository.class);
        ServiceRepository serviceRepository = context.getBean(ServiceRepository.class);
        SupplierRepository supplierRepository = context.getBean(SupplierRepository.class);
        SupplyRepository supplyRepository = context.getBean(SupplyRepository.class);
        WorkplaceRepository workplaceRepository = context.getBean(WorkplaceRepository.class);

        serviceRealizationRepository.findAll().forEach(serviceRealization -> logger.info(serviceRealization.toString()));
    }
}
