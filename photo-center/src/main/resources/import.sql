
INSERT INTO service(id, name, price) VALUES(1, 'Печать фото', 30);
INSERT INTO service(id, name, price) VALUES(2, 'Проявка фотопленки', 100);
INSERT INTO service(id, name, price) VALUES(3, 'Фото на документы', 200);
INSERT INTO service(id, name, price) VALUES(4, 'Реставрация фото', 100);
INSERT INTO service(id, name, price) VALUES(5, 'Художественное фото', 200);

INSERT INTO product(id, name, producer_firm, price) VALUES(1, 'Фотопленка', 'Kodak', 100);
INSERT INTO product(id, name, producer_firm, price) VALUES(2, 'Фотопленка', 'Nikon', 150);
INSERT INTO product(id, name, producer_firm, price) VALUES(3, 'Фотопленка', 'Canon', 200);
INSERT INTO product(id, name, producer_firm, price) VALUES(4, 'Фотоаппарат', 'Kodak', 8000);
INSERT INTO product(id, name, producer_firm, price) VALUES(5, 'Фотоаппарат', 'Nikon', 10000);
INSERT INTO product(id, name, producer_firm, price) VALUES(6, 'Фотоаппарат', 'Canon', 15000);
INSERT INTO product(id, name, producer_firm, price) VALUES(7, 'Фотоальбом', 'Kodak', 300);
INSERT INTO product(id, name, producer_firm, price) VALUES(8, 'Фотоальбом', 'Nikon', 500);
INSERT INTO product(id, name, producer_firm, price) VALUES(9, 'Фотоальбом', 'Canon', 700);

INSERT INTO client(id, last_name, first_name, discount) VALUES(1, 'Васильев', 'Василий', 20);
INSERT INTO client(id, last_name, first_name, discount) VALUES(2, 'Петров', 'Петр', 10);
INSERT INTO client(id, last_name, first_name, discount) VALUES(3, 'Сергеев', 'Сергей', 5);
INSERT INTO client(id, last_name, first_name, discount) VALUES(4, 'Никитин', 'Никита', 30);
INSERT INTO client(id, last_name, first_name, discount) VALUES(5, 'Павлов', 'Павел', NULL);

INSERT INTO supplier(id, last_name, first_name) VALUES(1, 'Александров', 'Александр');
INSERT INTO supplier(id, last_name, first_name) VALUES(2, 'Романов', 'Роман');
INSERT INTO supplier(id, last_name, first_name) VALUES(3, 'Алексеев', 'Алексей');

INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(1, 1, 1, 10, '24.05.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(2, 1, 2, 30, '03.08.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(3, 1, 3, 20, '11.11.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(4, 2, 4, 2, '10.10.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(5, 2, 5, 3, '10.10.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(6, 2, 6, 1, '10.10.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(7, 3, 7, 10, '24.05.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(8, 3, 8, 15, '03.07.2015');
INSERT INTO supply(id, supplier_id, product_id, amount, date) VALUES(9, 3, 9, 5, '02.02.2015');

INSERT INTO department_type(id, name, takes_urgent_orders) VALUES(1, 'Киоск', FALSE);
INSERT INTO department_type(id, name, takes_urgent_orders) VALUES(2, 'Магазин', TRUE);

INSERT INTO department_type_services(department_types_id, services_id) VALUES(1, 1);
INSERT INTO department_type_services(department_types_id, services_id) VALUES(1, 2);
INSERT INTO department_type_services(department_types_id, services_id) VALUES(2, 1);
INSERT INTO department_type_services(department_types_id, services_id) VALUES(2, 2);
INSERT INTO department_type_services(department_types_id, services_id) VALUES(2, 3);
INSERT INTO department_type_services(department_types_id, services_id) VALUES(2, 4);
INSERT INTO department_type_services(department_types_id, services_id) VALUES(2, 5);

INSERT INTO post(id, title) VALUES(1, 'Кассир');
INSERT INTO post(id, title) VALUES(2, 'Фотограф');
INSERT INTO post(id, title) VALUES(3, 'Администратор');
INSERT INTO post(id, title) VALUES(4, 'Дизайнер');
INSERT INTO post(id, title) VALUES(5, 'Оцифровщик');

INSERT INTO department(id, address, department_type_id, executor_id) VALUES(1, 'Ленина 266', 2, 1);
INSERT INTO department(id, address, department_type_id, executor_id) VALUES(2, 'Ильича 3', 2, 2);
INSERT INTO department(id, address, department_type_id, executor_id) VALUES(3, 'Мухачева 228', 1, 1);
INSERT INTO department(id, address, department_type_id, executor_id) VALUES(4, 'Кутателадзе 26', 1, 1);
INSERT INTO department(id, address, department_type_id, executor_id) VALUES(5, 'Орджоникидзе 105', 1, 2);

INSERT INTO workplace(id, post_id, department_id, amount) VALUES(1, 1, 1, 2);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(2, 2, 1, 2);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(3, 3, 1, 1);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(4, 4, 1, 2);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(5, 5, 1, 1);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(6, 1, 2, 1);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(7, 2, 2, 2);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(8, 3, 2, 1);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(9, 4, 2, 3);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(10, 5, 2, 2);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(11, 1, 3, 1);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(12, 1, 4, 1);
INSERT INTO workplace(id, post_id, department_id, amount) VALUES(13, 1, 5, 2);

INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(1, 1, 1, 2, 160, 1, '12.04.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(2, 5, 1, 1, 9500, 3, '18.04.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(3, 7, 1, 3, 900, 5, '12.07.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(4, 6, 2, 1, 13500, 2, '1.03.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(5, 8, 2, 2, 700, 4, '2.06.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(6, 2, 3, 3, 360, 1, '19.11.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(7, 3, 3, 3, 570, 3, '11.07.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(8, 4, 4, 1, 7200, 2, '21.05.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(9, 9, 4, 4, 2800, 5, '12.08.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(10, 1, 5, 5, 350, 4, '05.09.2015');
INSERT INTO product_realization(id, product_id, department_id, amount, total_price, client_id, date) VALUES(11, 5, 5, 2, 14000, 4, '08.09.2015');

INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(1, 1, 1, 100, TRUE, 3600, 'всех по 1, формат: А7, бумага: глянцевая', 1, '31.01.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(2, 3, 1, 1, FALSE, 190, NULL, 3, '20.04.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(3, 4, 1, 7, TRUE, 1400, NULL, 5, '06.07.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(4, 2, 2, 5, TRUE, 900, NULL, 2, '13.03.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(5, 4, 2, 6, FALSE, 420, NULL, 4, '18.02.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(6, 5, 2, 4, TRUE, 1520, NULL, 3, '05.09.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(7, 1, 3, 60, FALSE, 1260, 'всех по 2, формат: А5, бумага: матовая', 1, '17.08.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(8, 1, 3, 120, FALSE, 3240, 'всех по 1, формат: А6, бумага: матовая', 2, '18.05.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(9, 2, 3, 2, FALSE, 190, NULL, 3, '12.07.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(10, 2, 4, 10, FALSE, 700, NULL, 4, '4.10.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(11, 2, 4, 1, FALSE, 100, NULL, 5, '21.08.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(12, 1, 4, 40, FALSE, 1080, 'всех по 2, формат: А5, тип бумаги - глянец', 2, '03.03.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(13, 1, 5, 80, FALSE, 1920, 'всех по 1, формат: А6, тип бумаги - глянец', 1, '12.04.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(14, 2, 5, 5, FALSE, 350, NULL, 4, '30.09.2015');
INSERT INTO service_realization(id, service_id, department_id, amount, urgent, total_price, additional_info, client_id, date) VALUES(15, 1, 5, 200, FALSE, 6000, 'всех по 1, формат: А5, бумага: матовая', 5, '21.12.2015');
