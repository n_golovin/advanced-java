<#import "/spring.ftl" as spring/>
<@layout title="Supplies">
<div class="control-group">
    <h3 class="text-info">Supply</h3>
    <form name="supply-new" method="post">
        <fieldset>
            <div class="form-group">
                <@spring.bind  "supply.supplier.id" />
                <div class="control-group">
                    <label class="control-label" for="supplier.id">Supplier Id</label>
                </div>
                <div class="controls">
                    <input type="number" name="supplier.id" value="${supply.supplier.id!0}" min="1" required>
                </div>
                <#list spring.status.errorMessages as error>
                    <span class="text-danger">${error}<br/></span>
                </#list>
            </div>

            <div class="form-group">
                <@spring.bind  "supply.product.id" />
                <div class="control-group">
                    <label class="control-label" for="product.id">Product Id</label>
                </div>
                <div class="controls">
                    <input type="number" name="product.id" value="${supply.product.id!0}" min="1" required>
                </div>
                <#list spring.status.errorMessages as error>
                    <span class="text-danger">${error}<br/></span>
                </#list>
            </div>


            <div class="form-group">
                <@spring.bind  "supply.amount" />
                <div class="control-group">
                    <label class="control-label" for="amount">Amount</label>
                </div>
                <div class="controls">
                    <input type="number" name="amount" value="${supply.amount!0}" required>
                </div>
                <#list spring.status.errorMessages as error>
                    <span class="text-danger">${error}<br/></span>
                </#list>
            </div>

            <div class="form-group">
                <@spring.bind  "supply.date" />
                <div class="control-group">
                    <label class="control-label" for="date">Date</label>
                </div>
                <div class="controls">
                    <input type="text" name="date" value="${supply.date!""}" required>
                </div>
                <#list spring.status.errorMessages as error>
                    <span class="text-danger">${error}<br/></span>
                </#list>
            </div>

            <div class="controls">
                <input type="submit" value="Create" class="btn btn-primary">
            </div>
        </fieldset>
    </form>
</div>
</@layout>