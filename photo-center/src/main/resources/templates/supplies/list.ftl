<div>
    <#if supplies?size != 0 >
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Supplier Id</th>
                <th>Product Id</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <#list supplies as supply>
                <tr>
                    <td>${supply.id}</td>
                    <td>${supply.supplier.id}</td>
                    <td>${supply.product.id}</td>
                    <td>${supply.amount}</td>
                    <td>${supply.date}</td>
                    <td>
                        <form>
                            <button type="submit" formaction="${rc.getContextUrl('edit/${supply.id}/')}" formmethod="get" class="btn btn-default"><img src="/images/edit.png" /></button>
                            <button type="submit" formaction="${rc.getContextUrl('delete/${supply.id}/')}" formmethod="post" class="btn btn-default"><img src="/images/delete.png" /></button>
                        </form>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>
</div>