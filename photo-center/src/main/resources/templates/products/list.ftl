<div>
    <#if products?size != 0 >
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Price</th>
                <th>Producer Firm</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <#list products as product>
                <tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.price}</td>
                    <td>${product.producerFirm}</td>
                    <td>
                        <form>
                            <button type="submit" formaction="${rc.getContextUrl('edit/${product.id}/')}" formmethod="get" class="btn btn-default"><img src="/images/edit.png" /></button>
                            <button type="submit" formaction="${rc.getContextUrl('delete/${product.id}/')}" formmethod="post" class="btn btn-default"><img src="/images/delete.png" /></button>
                        </form>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>
</div>