<#import "/spring.ftl" as spring/>
<@layout title="Products">
    <div class="control-group">
        <h3 class="text-info">Product</h3>
        <form name="product-edit" method="post">
            <fieldset>
                <div class="form-group">
                    <div class="control-group">
                        <label class="control-label" for="id">Id</label>
                    </div>
                    <div class="controls">
                        <input type="number" name="id" value="${product.id}" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <@spring.bind  "product.name" />
                    <div class="control-group">
                        <label class="control-label" for="name">Name</label>
                    </div>
                    <div class="controls">
                        <input type="text" name="name" value="${product.name}">
                    </div>
                    <#list spring.status.errorMessages as error>
                        <span class="text-danger">${error}<br/></span>
                    </#list>
                </div>

                <div class="form-group">
                    <@spring.bind  "product.producerFirm" />
                    <div class="control-group">
                        <label class="control-label" for="producerFirm">Producer Firm</label>
                    </div>
                    <div class="controls">
                        <input type="text" name="producerFirm" value="${product.producerFirm}">
                    </div>
                    <#list spring.status.errorMessages as error>
                        <span class="text-danger">${error}<br/></span>
                    </#list>
                </div>

                <div class="form-group">
                    <@spring.bind  "product.price" />
                    <div class="control-group">
                        <label class="control-label" for="price">Price</label>
                    </div>
                    <div class="controls">
                        <input type="number" name="price" value="${product.price}" required>
                    </div>
                    <#list spring.status.errorMessages as error>
                        <span class="text-danger">${error}<br/></span>
                    </#list>
                </div>

                <div class="controls">
                    <input type="submit" value="Update" class="btn btn-primary">
                </div>
            </fieldset>
        </form>
    </div>
</@layout>