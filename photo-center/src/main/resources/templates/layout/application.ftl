<#macro layout title>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${title}</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <link rel="shortcut icon" href="/static/images/favicon.ico" type="image/x-icon">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a> </li>
                    <li><a href="/products/">Products</a> </li>
                    <li><a href="/suppliers/">Suppliers</a> </li>
                    <li><a href="/supplies/">Supplies</a> </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <#nested>
    </div>
</body>
</html>
</#macro>