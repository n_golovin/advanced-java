<#import "/spring.ftl" as spring/>
<@layout title="Suppliers">
<div class="control-group">
    <h3 class="text-info">Supplier</h3>
    <form name="supplier-new" method="post">
        <fieldset>
            <div class="form-group">
                <@spring.bind  "supplier.lastName" />
                <div class="control-group">
                    <label class="control-label" for="lastName">Last Name</label>
                </div>
                <div class="controls">
                    <input type="text" name="lastName" value="${supplier.lastName!""}">
                </div>
                <#list spring.status.errorMessages as error>
                    <span class="text-danger">${error}<br/></span>
                </#list>
            </div>

            <div class="form-group">
                <@spring.bind  "supplier.firstName" />
                <div class="control-group">
                    <label class="control-label" for="firstName">First Name</label>
                </div>
                <div class="controls">
                    <input type="text" name="firstName" value="${supplier.firstName!""}">
                </div>
                <#list spring.status.errorMessages as error>
                    <span class="text-danger">${error}<br/></span>
                </#list>
            </div>

            <div class="controls">
                <input type="submit" value="Create" class="btn btn-primary">
            </div>
        </fieldset>
    </form>
</div>
</@layout>