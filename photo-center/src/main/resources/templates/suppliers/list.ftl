<div>
    <#if suppliers?size != 0 >
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <#list suppliers as supplier>
                <tr>
                    <td>${supplier.id}</td>
                    <td>${supplier.lastName}</td>
                    <td>${supplier.firstName}</td>
                    <td>
                        <form>
                            <button type="submit" formaction="${rc.getContextUrl('edit/${supplier.id}/')}" formmethod="get" class="btn btn-default"><img src="/images/edit.png" /></button>
                            <button type="submit" formaction="${rc.getContextUrl('delete/${supplier.id}/')}" formmethod="post" class="btn btn-default"><img src="/images/delete.png" /></button>
                        </form>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </#if>
</div>