<@layout title="Suppliers">
<nav class="navbar navbar-static-top">
    <div class="container-fluid">
        <div class="collapse navbar-collapse">
            <a class="navbar-brand" href="">Supplier List</a>
            <ul class="nav navbar-nav">
                <li><a href="${rc.getContextUrl('new/')}">New</a></li>
                <li><a href="">Search</a></li>
            </ul>
        </div>
    </div>
</nav>
<#include "list.ftl">
</@layout>