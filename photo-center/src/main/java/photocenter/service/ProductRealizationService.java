package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Department;
import photocenter.entity.ProductRealization;
import photocenter.repository.ProductRealizationRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by n1k1t4 on 24.04.16.
 */

@Service
@Transactional
public class ProductRealizationService {
    @Autowired
    private ProductRealizationRepository productRealizationRepository;

    public ProductRealization save(ProductRealization productRealization) {
        return productRealizationRepository.saveAndFlush(productRealization);
    }

    void delete(ProductRealization productRealization) {
        productRealizationRepository.delete(productRealization);
    }

    public void delete(Long id) {
        productRealizationRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public ProductRealization findOne(Long id) {
        return productRealizationRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<ProductRealization> findAll() {
            return productRealizationRepository.findAll();
    }

    @Transactional(readOnly = true)
    public long getRevenueForPeriod(Date startDate, Date endDate) {
        return productRealizationRepository.sumTotalPriceForPeriod(startDate, endDate);
    }

    @Transactional(readOnly = true)
    public long getRevenueByDepartmentForPeriod(Department department, Date startDate, Date endDate) {
        return productRealizationRepository.sumTotalPriceByDepartmentForPeriod(department, startDate, endDate);
    }
}
