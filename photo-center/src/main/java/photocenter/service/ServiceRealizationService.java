package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Department;
import photocenter.entity.DepartmentType;
import photocenter.entity.ServiceRealization;
import photocenter.repository.ServiceRealizationRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by n1k1t4 on 24.04.16.
 */

@Service
@Transactional
public class ServiceRealizationService {
    @Autowired
    private ServiceRealizationRepository serviceRealizationRepository;

    public ServiceRealization save(ServiceRealization serviceRealization) {
        return serviceRealizationRepository.saveAndFlush(serviceRealization);
    }

    public void delete(ServiceRealization serviceRealization) {
        serviceRealizationRepository.delete(serviceRealization);
    }

    public void delete(Long id) {
        serviceRealizationRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public ServiceRealization findOne(Long id) {
        return serviceRealizationRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<ServiceRealization> findAll() {
        return serviceRealizationRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<ServiceRealization> findServicesRealizationsByDepartmentTypeForPeriod(DepartmentType departmentType, Date startDate, Date endDate) {
        return serviceRealizationRepository.findByDepartment_DepartmentTypeAndDateBetween(departmentType, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public long countServicesRealizationsByDepartmentTypeForPeriod(DepartmentType departmentType, Date startDate, Date endDate) {
        return serviceRealizationRepository.countByDepartment_DepartmentTypeAndDateBetween(departmentType, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public List<ServiceRealization> findServicesRealizationsByServiceAndDepartmentAndUrgentForPeriod(photocenter.entity.Service service,
                                                                                              Department department, boolean urgent,
                                                                                              Date startDate, Date endDate) {
        return serviceRealizationRepository.findByServiceAndDepartmentAndUrgentAndAndDateBetween(service, department,
                urgent, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public long countServicesRealizationsByServiceAndDepartmentAndUrgentForPeriod(photocenter.entity.Service service,
                                                                           Department department, boolean urgent,
                                                                           Date startDate, Date endDate) {
        return serviceRealizationRepository.countByServiceAndDepartmentAndUrgentAndDateBetween(service, department,
                urgent, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public long getRevenueByServiceAndDepartmentAndUrgentForPeriod(photocenter.entity.Service service, Department department, boolean urgent,
                                                            Date startDate, Date endDate) {
        return serviceRealizationRepository.sumTotalPriceByServiceAndDepartmentForPeriod(service, department, urgent, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public long getTotalAmountOfServiceRealizationForPeriod(photocenter.entity.Service service, Date startDate, Date endDate) {
        return serviceRealizationRepository.sumAmountByServiceForPeriod(service, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public long getTotalAmountOfServiceRealizationByDepartmentForPeriod(photocenter.entity.Service service, Department department,
                                                                 Date startDate, Date endDate) {
        return serviceRealizationRepository.sumAmountByServiceAndDepartmentForPeriod(service, department, startDate, endDate);
    }
}
