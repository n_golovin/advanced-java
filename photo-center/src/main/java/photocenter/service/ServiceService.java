package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Service;
import photocenter.repository.ServiceRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@org.springframework.stereotype.Service
@Transactional
public class ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;

    public Service save(Service service) {
        return serviceRepository.saveAndFlush(service);
    }

    public void delete(Service service) {
        serviceRepository.delete(service);
    }

    public void delete(Long id) {
        serviceRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public photocenter.entity.Service findOne(Long id) {
        return serviceRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Service> findAll() {
        return serviceRepository.findAll();
    }
}
