package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Post;
import photocenter.entity.Workplace;
import photocenter.repository.WorkplaceRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 19.04.16.
 */

@Service
@Transactional
public class WorkplaceService {
    @Autowired
    private WorkplaceRepository workplaceRepository;

    public Workplace save(Workplace workplace) {
        return workplaceRepository.saveAndFlush(workplace);
    }

    public void delete(Workplace workplace) {
        workplaceRepository.delete(workplace);
    }

    public void delete(Long id) {
        workplaceRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Workplace findOne(Long id) {
        return workplaceRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Workplace> findAll() {
        return workplaceRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Workplace> findWorkplacesByPost(Post post) {
        return workplaceRepository.findByPost(post);
    }
}
