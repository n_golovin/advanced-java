package photocenter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Department;
import photocenter.entity.Product;
import photocenter.repository.ProductRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Service
@Transactional
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public Product save(Product product) {
        Logger logger = LoggerFactory.getLogger(ProductService.class);
        logger.info(String.valueOf(product.getId()));
        return productRepository.saveAndFlush(product);
    }

    public void delete(Product product) {
        productRepository.delete(product);
    }

    public void delete(Long id) {
        productRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Product find(Long id) {
        return productRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Product> findProductsWhichAreMostPopular() {
        return productRepository.findTop5ByOrderByProductsRealizations_AmountDesc();
    }

    @Transactional(readOnly = true)
    public List<Product> findProductsWhichAreMostPopularInDepartment(Department department) {
        return productRepository.findTop5ByProductsRealizations_DepartmentOrderByProductsRealizations_AmountDesc(department);
    }
}
