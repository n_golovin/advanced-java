package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Supply;
import photocenter.repository.SupplyRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 24.04.16.
 */

@Service
@Transactional
public class SupplyService {
    @Autowired
    private SupplyRepository supplyRepository;

    public Supply save(Supply supply) {
        return supplyRepository.saveAndFlush(supply);
    }

    public void delete(Supply supply) {
        supplyRepository.delete(supply);
    }

    public void delete(Long id) {
        supplyRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Supply find(Long id) {
        return supplyRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Supply> findAll() {
        return supplyRepository.findAll();
    }
}
