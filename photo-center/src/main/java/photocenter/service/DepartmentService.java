package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Department;
import photocenter.entity.DepartmentType;
import photocenter.repository.DepartmentRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Service
@Transactional
public class DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;

    public Department save(Department department) {
        return departmentRepository.saveAndFlush(department);
    }

    public void delete(Department department) {
        departmentRepository.delete(department);
    }

    public void delete(Long id) {
        departmentRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Department findOne(Long id) {
        return departmentRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Department> findDepartmentsByDepartmentType(DepartmentType departmentType) {
        return departmentRepository.findByDepartmentType(departmentType);
    }

    @Transactional(readOnly = true)
    public long countDepartmentsByDepartmentType(DepartmentType departmentType) {
        return departmentRepository.countByDepartmentType(departmentType);
    }
}
