package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.DepartmentType;
import photocenter.repository.DepartmentTypeRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 24.04.16.
 */

@Service
@Transactional
public class DepartmentTypeService {
    @Autowired
    private DepartmentTypeRepository departmentTypeRepository;

    public DepartmentType save(DepartmentType departmentType) {
        return departmentTypeRepository.saveAndFlush(departmentType);
    }

    public void delete(DepartmentType departmentType) {
        departmentTypeRepository.delete(departmentType);
    }

    public void delete(Long id) {
        departmentTypeRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public DepartmentType findOne(Long id) {
        return departmentTypeRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<DepartmentType> findAll() {
        return departmentTypeRepository.findAll();
    }
}
