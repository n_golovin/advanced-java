package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Post;
import photocenter.repository.PostRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 24.04.16.
 */

@Service
@Transactional
public class PostService {
    @Autowired
    private PostRepository postRepository;

    public Post save(Post post) {
        return postRepository.saveAndFlush(post);
    }

    public void delete(Post post) {
        postRepository.delete(post);
    }

    public void delete(Long id) {
        postRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Post findOne(Long id) {
        return postRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Post> findAll() {
        return postRepository.findAll();
    }
}
