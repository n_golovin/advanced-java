package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Product;
import photocenter.entity.Supplier;
import photocenter.repository.SupplierRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Service
@Transactional
public class SupplierService {
    @Autowired
    private SupplierRepository supplierRepository;

    public Supplier save(Supplier supplier) {
        return supplierRepository.saveAndFlush(supplier);
    }

    public void delete(Supplier supplier) {
        supplierRepository.delete(supplier);
    }

    public void delete(Long id) {
        supplierRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Supplier find(Long id) {
        return supplierRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Supplier> findAll() {
        return supplierRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Supplier> findSuppliersOfProducts(List<Product> products) {
        return supplierRepository.findDistinctBySupplies_ProductIn(products);
    }

    @Transactional(readOnly = true)
    public List<Supplier> findSuppliersWhoHaveMadeSuppliesForPeriod(Date startDate, Date endDate) {
        return supplierRepository.findDistinctBySupplies_DateBetween(startDate, endDate);
    }

    @Transactional(readOnly = true)
    public List<Supplier> findSuppliersWhoMadeSuppliesOfAmount(int startAmount, int endAmount) {
        return supplierRepository.findDistinctBySupplies_AmountBetween(startAmount, endAmount);
    }
}
