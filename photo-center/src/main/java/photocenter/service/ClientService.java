package photocenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import photocenter.entity.Client;
import photocenter.repository.ClientRepository;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Service
@Transactional
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public Client save(Client client) {
        return clientRepository.saveAndFlush(client);
    }

    public void delete(Client client) {
        clientRepository.delete(client);
    }

    public void delete(Long id) {
        clientRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public Client findOne(Long id) {
        return clientRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Client> findClientsWhoHaveDiscount() {
        return clientRepository.findByDiscountIsNotNull();
    }
}
