package photocenter.dto;

import photocenter.entity.Product;

/**
 * Created by n1k1t4 on 26.04.16.
 */

public class ProductRealizationDTO {
    private Product product;

    private long productRealizationsAmount;

    public ProductRealizationDTO() {
    }

    public ProductRealizationDTO(Product product, long productRealizationsAmount) {
        this.product = product;
        this.productRealizationsAmount = productRealizationsAmount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getProductRealizationsAmount() {
        return productRealizationsAmount;
    }

    public void setProductRealizationsAmount(long productRealizationsAmount) {
        this.productRealizationsAmount = productRealizationsAmount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if(obj instanceof ProductRealizationDTO){
            ProductRealizationDTO productRealizationDTO = (ProductRealizationDTO) obj;

            if (!getProduct().getId().equals(productRealizationDTO.getProduct().getId())) {
                return false;
            }

            if (getProductRealizationsAmount() != productRealizationDTO.getProductRealizationsAmount()) {
                return false;
            }

            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return getProduct().hashCode() + (int) productRealizationsAmount;
    }

    @Override
    public String toString() {
        return String.join(", ",
                getProduct().toString(),
                "productRealizationsAmount: " + getProductRealizationsAmount());
    }
}
