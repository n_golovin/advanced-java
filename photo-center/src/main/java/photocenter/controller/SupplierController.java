package photocenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import photocenter.entity.Supplier;
import photocenter.service.SupplierService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by n1k1t4 on 08.05.16.
 */

@Controller
@RequestMapping("/suppliers/")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView("suppliers/root");
        List<Supplier> suppliers = supplierService.findAll().stream()
                .sorted((o1, o2) -> o1.getId().compareTo(o2.getId()))
                .collect(Collectors.toList());
        modelAndView.addObject("suppliers", suppliers);

        return modelAndView;
    }

    @RequestMapping(value = "/new/", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("suppliers/new");
        modelAndView.addObject("supplier", new Supplier());

        return modelAndView;
    }

    @RequestMapping(value = "/new/", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute("supplier") @Valid Supplier supplier, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("suppliers/new");
            modelAndView.addAllObjects(bindingResult.getModel());

            return modelAndView;
        }

        supplierService.save(supplier);

        return new ModelAndView("redirect:/suppliers/");
    }

    @RequestMapping(value = "/edit/{id}/", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("suppliers/edit");
        Supplier supplier = supplierService.find(id);
        modelAndView.addObject("supplier", supplier);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute("supplier") @Valid Supplier supplier, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("suppliers/edit");
            modelAndView.addAllObjects(bindingResult.getModel());

            return modelAndView;
        }

        supplierService.save(supplier);

        return new ModelAndView("redirect:/suppliers/");
    }

    @RequestMapping(value = "/delete/{id}/", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable("id") Long id) {
        supplierService.delete(id);

        return new ModelAndView("redirect:/suppliers/");
    }
}

