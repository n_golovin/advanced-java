package photocenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import photocenter.entity.Product;
import photocenter.entity.Supplier;
import photocenter.entity.Supply;
import photocenter.service.ProductService;
import photocenter.service.SupplierService;
import photocenter.service.SupplyService;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Created by n1k1t4 on 08.05.16.
 */

@Controller
@RequestMapping("/supplies/")
public class SupplyController {
    @Autowired
    private SupplyService supplyService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView("supplies/root");
        List<Supply> supplies = supplyService.findAll().stream()
                .sorted((o1, o2) -> o1.getId().compareTo(o2.getId()))
                .collect(Collectors.toList());
        modelAndView.addObject("supplies", supplies);

        return modelAndView;
    }

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH);
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, "date", new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping(value = "/new/", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("supplies/new");
        modelAndView.addObject("supply", new Supply());

        return modelAndView;
    }

    @RequestMapping(value = "/new/", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute("supply") @Valid Supply supply, BindingResult bindingResult) {
        Supplier supplier = supplierService.find(supply.getSupplier().getId());
        if (supplier == null) {
            bindingResult.rejectValue("supplier.id", "error.supply", "Supplier with specified id does not exist");
        }
        Product product = productService.find(supply.getProduct().getId());
        if (product == null) {
            bindingResult.rejectValue("product.id", "error.supply", "Product with specified id does not exist");
        }

        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("supplies/new");
            modelAndView.addAllObjects(bindingResult.getModel());

            return modelAndView;
        }

        supply.setSupplier(supplier);
        supply.setProduct(product);
        supplyService.save(supply);

        return new ModelAndView("redirect:/supplies/");
    }

    @RequestMapping(value = "/edit/{id}/", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("supplies/edit");
        Supply supply = supplyService.find(id);
        modelAndView.addObject("supply", supply);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute("supply") @Valid Supply supply, BindingResult bindingResult) {
        Supplier supplier = supplierService.find(supply.getSupplier().getId());
        if (supplier == null) {
            bindingResult.rejectValue("supplier.id", "error.supply", "Supplier with specified id does not exist");
        }
        Product product = productService.find(supply.getProduct().getId());
        if (product == null) {
            bindingResult.rejectValue("product.id", "error.supply", "Product with specified id does not exist");
        }

        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("supplies/edit");
            modelAndView.addAllObjects(bindingResult.getModel());

            return modelAndView;
        }

        supply.setSupplier(supplier);
        supply.setProduct(product);
        supplyService.save(supply);

        return new ModelAndView("redirect:/supplies/");
    }

    @RequestMapping(value = "/delete/{id}/", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable("id") Long id) {
        supplyService.delete(id);

        return new ModelAndView("redirect:/supplies/");
    }
}
