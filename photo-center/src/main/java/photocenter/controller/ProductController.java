package photocenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import photocenter.entity.Product;
import photocenter.service.ProductService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by n1k1t4 on 08.05.16.
 */

@Controller
@RequestMapping("/products/")
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView("products/root");
        List<Product> products = productService.findAll().stream()
                .sorted((o1, o2) -> o1.getId().compareTo(o2.getId()))
                .collect(Collectors.toList());
        modelAndView.addObject("products", products);

        return modelAndView;
    }

    @RequestMapping(value = "/new/", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("products/new");
        modelAndView.addObject("product", new Product());

        return modelAndView;
    }

    @RequestMapping(value = "/new/", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute("product") @Valid Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("products/new");

            return modelAndView;
        }

        productService.save(product);

        return new ModelAndView("redirect:/products/");
    }

    @RequestMapping(value = "/edit/{id}/", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("products/edit");
        Product product = productService.find(id);
        modelAndView.addObject("product", product);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute("product") @Valid Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("products/edit");
            modelAndView.addAllObjects(bindingResult.getModel());

            return modelAndView;
        }

        productService.save(product);

        return new ModelAndView("redirect:/products/");
    }

    @RequestMapping(value = "/delete/{id}/", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable("id") Long id) {
        productService.delete(id);

        return new ModelAndView("redirect:/products/");
    }
}
