package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Department;
import photocenter.entity.Product;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findTop5ByOrderByProductsRealizations_AmountDesc();

    List<Product> findTop5ByProductsRealizations_DepartmentOrderByProductsRealizations_AmountDesc(Department department);
}
