package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Post;

/**
 * Created by n1k1t4 on 19.04.16.
 */

public interface PostRepository extends JpaRepository<Post, Long> {
}
