package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import photocenter.entity.Department;
import photocenter.entity.DepartmentType;
import photocenter.entity.Service;
import photocenter.entity.ServiceRealization;

import java.util.Date;
import java.util.List;

/**
 * Created by n1k1t4 on 19.04.16.
 */

public interface ServiceRealizationRepository extends JpaRepository<ServiceRealization, Long> {
    List<ServiceRealization> findByDepartment_DepartmentTypeAndDateBetween(DepartmentType departmentType,
                                                                           Date startDate, Date endDate);

    long countByDepartment_DepartmentTypeAndDateBetween(DepartmentType departmentType,
                                                         Date startDate, Date endDate);


    List<ServiceRealization> findByServiceAndDepartmentAndUrgentAndAndDateBetween(Service service,
                                                                                  Department department,
                                                                                  boolean urgent,
                                                                                  Date startDate, Date endDate);

    long countByServiceAndDepartmentAndUrgentAndDateBetween(Service service,
                                                            Department department,
                                                            boolean urgent,
                                                            Date startDate, Date endDate);


    @Query("SELECT SUM(sr.totalPrice) FROM ServiceRealization sr WHERE sr.service = :service AND " +
            "sr.department = :department AND sr.urgent = :urgent AND sr.date BETWEEN :startDate AND :endDate")
    long sumTotalPriceByServiceAndDepartmentForPeriod(@Param("service") Service service,
                                                      @Param("department") Department department,
                                                      @Param("urgent") boolean urgent,
                                                      @Param("startDate") Date startDate,
                                                      @Param("endDate") Date endDate);

    @Query("SELECT SUM(sr.amount) FROM ServiceRealization sr WHERE sr.service = :service AND sr.date BETWEEN :startDate AND :endDate")
    long sumAmountByServiceForPeriod(@Param("service") Service service,
                                     @Param("startDate") Date startDate,
                                     @Param("endDate") Date endDate);

    @Query("SELECT SUM(sr.amount) FROM ServiceRealization sr WHERE sr.service = :service AND " +
            "sr.department = :department AND sr.date BETWEEN :startDate AND :endDate")
    long sumAmountByServiceAndDepartmentForPeriod(@Param("service") Service service,
                                                  @Param("department") Department department,
                                                  @Param("startDate") Date startDate,
                                                  @Param("endDate") Date endDate);
}
