package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Product;
import photocenter.entity.Supplier;

import java.util.Date;
import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
    List<Supplier> findDistinctBySupplies_AmountBetween(int startAmount, int endAmount);

    List<Supplier> findDistinctBySupplies_DateBetween(Date startDate, Date endDate);

    List<Supplier> findDistinctBySupplies_ProductIn(List<Product> products);
}
