package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Client;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

public interface ClientRepository extends JpaRepository<Client, Long> {
    List<Client> findByDiscountIsNotNull();
}
