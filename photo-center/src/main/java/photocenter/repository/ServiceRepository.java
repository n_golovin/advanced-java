package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Service;

/**
 * Created by n1k1t4 on 17.04.16.
 */

public interface ServiceRepository extends JpaRepository<Service, Long> {
}
