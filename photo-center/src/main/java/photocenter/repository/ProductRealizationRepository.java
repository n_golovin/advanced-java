package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import photocenter.dto.ProductRealizationDTO;
import photocenter.entity.Client;
import photocenter.entity.Department;
import photocenter.entity.ProductRealization;

import java.util.Date;
import java.util.List;

/**
 * Created by n1k1t4 on 19.04.16.
 */

public interface ProductRealizationRepository extends JpaRepository<ProductRealization, Long> {
    @Query("SELECT SUM(pr.totalPrice) FROM ProductRealization pr WHERE pr.date BETWEEN :startDate AND :endDate")
    long sumTotalPriceForPeriod(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("SELECT SUM(pr.totalPrice) FROM ProductRealization pr WHERE pr.department = :department AND " +
            "pr.date BETWEEN :startDate AND :endDate")
    long sumTotalPriceByDepartmentForPeriod(@Param("department") Department department,
                                            @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("SELECT NEW photocenter.dto.ProductRealizationDTO(pr.product, SUM(pr.amount)) FROM ProductRealization pr GROUP BY pr.product")
    List<ProductRealizationDTO> findProductsAndProductsRealizationsAmounts();

    @Procedure(name = "ProductRealization.calcProductRealizationTotalPrice", procedureName = "calcProductRealizationTotalPrice")
    Integer calcProductRealizationTotalPrice(@Param("client") Client client);

    @Procedure(name = "ProductRealization.hello", procedureName = "hello")
    Integer hello(@Param("value") Integer value);
}
