package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Supply;

/**
 * Created by n1k1t4 on 17.04.16.
 */

public interface SupplyRepository extends JpaRepository<Supply, Long> {
}
