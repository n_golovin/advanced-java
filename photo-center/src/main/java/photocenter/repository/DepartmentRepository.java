package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Department;
import photocenter.entity.DepartmentType;

import java.util.List;

/**
 * Created by n1k1t4 on 17.04.16.
 */

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    List<Department> findByDepartmentType(DepartmentType departmentType);

    long countByDepartmentType(DepartmentType departmentType);
}
