package photocenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import photocenter.entity.Post;
import photocenter.entity.Workplace;

import java.util.List;

/**
 * Created by n1k1t4 on 19.04.16.
 */

public interface WorkplaceRepository extends JpaRepository<Workplace, Long> {
    List<Workplace> findByPost(Post post);
}
