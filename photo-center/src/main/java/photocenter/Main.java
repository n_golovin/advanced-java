package photocenter;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by n1k1t4 on 08.05.16.
 */

@SpringBootApplication
public class Main {
    @Bean
    CommandLineRunner init() {
        return (args)-> {
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
