package photocenter.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "supplier_id", "product_id", "date" }))
public class Supply {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Supplier supplier;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Product product;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date date;

    @Min(value = 1, message = "Supply amount must be greater than zero")
    @Column(nullable = false)
    private int amount;

    public Supply() {
    }

    public Supply(Supplier supplier, Product product, Date date, int amount) {
        this.supplier = supplier;
        this.product = product;
        this.date = date;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "product: " + getProduct(),
                "supplier: " + getSupplier(),
                "amount: " + getAmount(),
                "date: " + getDate());
    }
}
