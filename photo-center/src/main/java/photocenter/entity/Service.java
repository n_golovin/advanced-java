package photocenter.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private int price;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "service", cascade = CascadeType.ALL)
    private Set<ServiceRealization> servicesRealizations = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "services")
    private Set<DepartmentType> departmentTypes;

    public Service() {
    }

    public Service(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Set<ServiceRealization> getServicesRealizations() {
        return servicesRealizations;
    }

    public void setServicesRealizations(Set<ServiceRealization> servicesRealizations) {
        this.servicesRealizations = servicesRealizations;
    }

    public Set<DepartmentType> getDepartmentTypes() {
        return departmentTypes;
    }

    public void setDepartmentTypes(Set<DepartmentType> departmentTypes) {
        this.departmentTypes = departmentTypes;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "name: " + getName(),
                "price: " + getPrice());
    }
}
