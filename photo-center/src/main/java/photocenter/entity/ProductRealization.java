package photocenter.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by n1k1t4 on 19.04.16.
 */

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "product_id", "client_id", "department_id", "date" }))
@NamedStoredProcedureQuery(name = "ProductRealization.calcProductRealizationTotalPrice", procedureName = "calcProductRealizationTotalPrice", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "client", type = Client.class),
        @StoredProcedureParameter(mode = ParameterMode.OUT, name = "totalPrice", type = Integer.class) })
//@NamedStoredProcedureQuery(name = "ProductRealization.hello", procedureName = "hello", parameters = {
//        @StoredProcedureParameter(mode = ParameterMode.IN, name = "value", type = Integer.class),
//        @StoredProcedureParameter(mode = ParameterMode.OUT, name = "price", type = Integer.class) })
public class ProductRealization {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Client client;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Department department;

    @Column(nullable = false)
    private Date date;


    @Column(nullable = false)
    private int amount;

    @Column(nullable = false)
    private int totalPrice;

    public ProductRealization() {
    }

    public ProductRealization(Product product, Client client, Department department, Date date, int amount, int totalPrice) {
        this.product = product;
        this.client = client;
        this.department = department;
        this.date = date;
        this.amount = amount;
        this.totalPrice = totalPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "product: " + getProduct(),
                "department: " + getDepartment(),
                "amount: " + getAmount(),
                "totalPrice: " + getTotalPrice(),
                "client: " + getClient(),
                "date: " + getDate());
    }
}
