package photocenter.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private DepartmentType departmentType;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Department executor;

    @Column(nullable = false)
    private String address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    private Set<ServiceRealization> servicesRealizations = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    private Set<ProductRealization> productsRealizations = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    private Set<Workplace> workplaces = new HashSet<>();

    public Department() {
    }

    public Department(DepartmentType departmentType, Department executor, String address) {
        this.departmentType = departmentType;
        this.executor = executor;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DepartmentType getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(DepartmentType departmentType) {
        this.departmentType = departmentType;
    }

    public Department getExecutor() {
        return executor;
    }

    public void setExecutor(Department executor) {
        this.executor = executor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<ServiceRealization> getServicesRealizations() {
        return servicesRealizations;
    }

    public void setServicesRealizations(Set<ServiceRealization> servicesRealizations) {
        this.servicesRealizations = servicesRealizations;
    }

    public Set<ProductRealization> getProductsRealizations() {
        return productsRealizations;
    }

    public void setProductsRealizations(Set<ProductRealization> productsRealizations) {
        this.productsRealizations = productsRealizations;
    }

    public Set<Workplace> getWorkplaces() {
        return workplaces;
    }

    public void setWorkplaces(Set<Workplace> workplaces) {
        this.workplaces = workplaces;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "address: " + getAddress(),
                "departmentType: " + getDepartmentType(),
                "executor: " + getExecutor());
    }
}
