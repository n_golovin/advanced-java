package photocenter.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
public class Supplier implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotBlank(message = "Please, enter supplier last name")
    @Column(nullable = false, length = 50)
    private String lastName;

    @NotBlank(message = "Please, enter supplier first name")
    @Column(nullable = false, length = 50)
    private String firstName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "supplier", cascade = CascadeType.ALL)
    private Set<Supply> supplies = new HashSet<>();

    public Supplier() {
    }

    public Supplier(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Set<Supply> getSupplies() {
        return supplies;
    }

    public void setSupplies(Set<Supply> supplies) {
        this.supplies = supplies;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "lastName: " + getLastName(),
                "firstName: " + getFirstName());
    }
}
