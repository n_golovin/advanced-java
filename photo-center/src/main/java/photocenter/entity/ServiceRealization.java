package photocenter.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by n1k1t4 on 19.04.16.
 */

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "service_id", "client_id", "department_id", "date" }))
public class ServiceRealization {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Service service;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Client client;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Department department;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false)
    private boolean urgent;

    @Column(nullable = false)
    private int amount;

    @Column(nullable = false)
    private int totalPrice;

    @Column
    private String additionalInfo;

    public ServiceRealization() {
    }

    public ServiceRealization(Service service, Client client, Department department, Date date,
                              boolean urgent, int amount, int totalPrice, String additionalInfo) {
        this.service = service;
        this.client = client;
        this.department = department;
        this.date = date;
        this.urgent = urgent;
        this.amount = amount;
        this.totalPrice = totalPrice;
        this.additionalInfo = additionalInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "service: " + getService(),
                "department: " + getDepartment(),
                "amount: " + getAmount(),
                "urgent: " + isUrgent(),
                "totalPrice: " + getTotalPrice(),
                "additionalInfo: " + getAdditionalInfo(),
                "client: " + getClient(),
                "date: " + getDate());
    }
}
