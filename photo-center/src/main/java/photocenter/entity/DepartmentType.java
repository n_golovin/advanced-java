package photocenter.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
public class DepartmentType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean takesUrgentOrders;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "departmentType", cascade = CascadeType.ALL)
    private Set<Department> departments = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Service> services;

    public DepartmentType() {
    }

    public DepartmentType(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTakesUrgentOrders() {
        return takesUrgentOrders;
    }

    public void setTakesUrgentOrders(boolean takesUrgentOrders) {
        this.takesUrgentOrders = takesUrgentOrders;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "name: " + getName(),
                "takesUrgentOrders: " + isTakesUrgentOrders());
    }
}
