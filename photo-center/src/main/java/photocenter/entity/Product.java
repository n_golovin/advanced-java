package photocenter.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotBlank(message = "Please, enter product name")
    @Column(nullable = false)
    private String name;

    @NotBlank(message = "Please, enter product producer firm")
    @Column(nullable = false)
    private String producerFirm;

    @Min(value = 0, message = "Price must be positive number")
    @Column(nullable = false)
    private int price;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private Set<Supply> supplies = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private Set<ProductRealization> productsRealizations = new HashSet<>();

    public Product() {
    }

    public Product(String name, String producerFirm, int price) {
        this.name = name;
        this.producerFirm = producerFirm;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducerFirm() {
        return producerFirm;
    }

    public void setProducerFirm(String producerFirm) {
        this.producerFirm = producerFirm;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Set<Supply> getSupplies() {
        return supplies;
    }

    public void setSupplies(Set<Supply> supplies) {
        this.supplies = supplies;
    }

    public Set<ProductRealization> getProductsRealizations() {
        return productsRealizations;
    }

    public void setProductsRealizations(Set<ProductRealization> productsRealizations) {
        this.productsRealizations = productsRealizations;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "name: " + getName(),
                "producerFirm: " + getProducerFirm(),
                "price: " + getPrice());
    }
}
