package photocenter.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by n1k1t4 on 17.04.16.
 */

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false, length = 50)
    private String lastName;

    @Column(nullable = false, length = 50)
    private String firstName;

    @Column
    private Integer discount;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private Set<ServiceRealization> servicesRealizations = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
    private Set<ProductRealization> productsRealizations = new HashSet<>();

    public Client() {
    }

    public Client(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Client(String lastName, String firstName, int discount) {
        this(lastName, firstName);
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Set<ServiceRealization> getServicesRealizations() {
        return servicesRealizations;
    }

    public void setServicesRealizations(Set<ServiceRealization> servicesRealizations) {
        this.servicesRealizations = servicesRealizations;
    }

    public Set<ProductRealization> getProductsRealizations() {
        return productsRealizations;
    }

    public void setProductsRealizations(Set<ProductRealization> productsRealizations) {
        this.productsRealizations = productsRealizations;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "lastName: " + getLastName(),
                "firstName: " + getFirstName(),
                "discount: " + getDiscount());
    }
}
