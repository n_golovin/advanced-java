package photocenter.entity;

import javax.persistence.*;

/**
 * Created by n1k1t4 on 19.04.16.
 */

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "department_id", "post_id" }))
public class Workplace {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Department department;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Post post;

    @Column(nullable = false)
    private int amount;

    public Workplace() {
    }

    public Workplace(Department department, Post post, int amount) {
        this.department = department;
        this.post = post;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.join(", ",
                "id: " + getId(),
                "post: " + getPost().getTitle(),
                "department: " + getDepartment(),
                "amount: " + getAmount());
    }
}
